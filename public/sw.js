const CACHE = "portfolio-2.0.0";
const ASSETS = ["index.html", "main.js", "styles.css", "manifest.json"];

self.addEventListener("install", (event) => {
  // Create a new cache and add all files to it
  async function addFilesToCache() {
    const cache = await caches.open(CACHE);
    await cache.addAll(ASSETS);
  }
  event.waitUntil(addFilesToCache());
});

self.addEventListener("activate", (event) => {
  // Remove previous cached data from disk
  async function deleteOldCaches() {
    for (const key of await caches.keys()) {
      if (key !== CACHE) await caches.delete(key);
    }
  }
  event.waitUntil(deleteOldCaches());
});

self.addEventListener("fetch", (event) => {
  async function respond() {
    const url = new URL(event.request.url);
    const cache = await caches.open(CACHE);
    const cachedResponse = await cache.match(event.request);
    // `build`/`files` can always be served from the cache.
    if (ASSETS.includes(url.pathname) && cachedResponse) return cachedResponse;
    // For everything else, try the network first, but fall back to the cache if we're offline.
    try {
      const response = await fetch(event.request);
      if (response.status === 200) cache.put(event.request, response.clone());
      return response;
    } catch (error) {
      if (!cachedResponse) throw error;
      return cachedResponse;
    }
  }
  event.respondWith(respond());
});
