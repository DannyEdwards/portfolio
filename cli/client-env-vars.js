import { writeFileSync } from "fs";

const CLIENT_PREFIX = "CLIENT_";
const contents = Object.entries(process.env).reduce(concatClientEnvVars, "");

/**
@param {string} carry
@param {[string, string]} current
*/
function concatClientEnvVars(carry, [key, value]) {
  if (key.indexOf(CLIENT_PREFIX) === 0)
    return carry + `export const ${key} = "${value}";\n`;
  return carry;
}

if (contents) {
  writeFileSync("env.ts", contents);
  console.log("Client environment variables written to `env.ts`.");
} else {
  console.log("No client environment variables two write.");
}
