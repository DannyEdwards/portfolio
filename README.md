# Dan's Personal Site

[![pipeline status](https://gitlab.com/DannyEdwards/portfolio/badges/master/pipeline.svg)](https://gitlab.com/DannyEdwards/portfolio/-/commits/master)

Just your bog-standard contact page for Dan Edwards, a web developer from the UK. There's also a few things here that I created at work and in my spare time, have a look if you like.

## Tech

The site is built using plain ol' HTML, CSS, and JS, and hosted with [GitLab pages](https://about.gitlab.com/features/pages/).

Scripts are written in functional TypeScript, bundled with [esbuild](https://esbuild.github.io/).

Cool patterns from [https://css-pattern.com/](https://css-pattern.com/).
